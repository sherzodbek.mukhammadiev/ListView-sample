package net.dasturlash.dilshod1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Sherzodbek on 19.03.2017.
 */

public class MyAdapter extends BaseAdapter {
    List<MyData> data;

    public MyAdapter(List<MyData> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item,viewGroup,false);
        TextView header = (TextView) view.findViewById(R.id.header_text);
        TextView subText = (TextView) view.findViewById(R.id.sub_text);
        TextView count = (TextView) view.findViewById(R.id.count);
        MyData t = data.get(i);//a.get(i).
        header.setText(t.getHeader());
        subText.setText(t.getSubText());
        count.setText(""+(i+1));
        return view;
    }
}
