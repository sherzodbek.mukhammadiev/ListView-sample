package net.dasturlash.dilshod1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {
    private List<MyData> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ListView list = (ListView) findViewById(R.id.list);
        data = getLoadedData();
        MyAdapter adapter = new MyAdapter(data);
        list.setAdapter(adapter);
    }

    private List<MyData> getLoadedData(){
        List<MyData> t = new ArrayList<MyData>();
        for(int i=0;i<1000;i++){
            t.add(new MyData("Header"+i,"subText"+i));
        }
        return t;
    }
}
